## Настройка и конфигурация кластера Kubernetes

### Развертывание кластера Kubernetes

Директория `mycluster` содержит конфигурацию для развертывания кластера 
Kubernetes через Kuberspray.

Для развертывания необходимо:
- склонировать репозиторий https://github.com/kubernetes-sigs/kubespray,
- установить Ansible и зависимости из файла requirements.txt,
- разместить директорию `mycluster` по пути `inventory/`

Пример команды развертывания:

`$ ansible-playbook -i inventory/mycluster/hosts.yaml  --become --become-user=root cluster.yml`
 

### Развертывание системы мониторинга

Директория `kube-prometheus-stack` содержит Prometheus Operator helm chart.

Команда установки helm chart в кластер:

`$ helm install netology-monitoring ./kube-prometheus-stack`